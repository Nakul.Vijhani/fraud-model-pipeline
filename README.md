# Fraud Model Pipeline

- Created by Nakul Vijhani

# 1. Functions to Prepare Data for Model Training (Fraud and Anomaly).ipynb
    In this notebook, I ensure that the necessary CSV files are available in the same directory for seamless data preparation. These files include:
    • BorPostalCodes_ann.csv
    • PropPostalCodes_ann.csv
    • Manifold_ann.csv
    • postal_code.csv
    I use several custom functions for data processing:
    • calc_min_max_dist: This function calculates the minimum and maximum distances, possibly between various geographical points related to our data.
    • convert_postal: It's likely used for converting or formatting postal code data into a usable format.
    • geo_distance: This function computes geographical distances, possibly between borrower locations and other relevant points.
    • calc_min_max_diff and income_diff: These functions calculate differences in various financial metrics, which could be crucial for identifying anomalies in our data.
    For model training, I use features like PRD_GIFT, DOWNPAYMENT, BLTV, TDSR, GDSR, and many others, which provide a comprehensive view of each loan's profile.
    Significantly, I split the data based on time, with the training set comprising data before 2023-01-01 and the test set data after this date. This split strategy is aimed at enhancing the model's performance in out-of-time predictions.


# 2. Model Training.ipynb
    In this file, I carefully select features to train the XGBoost model. The features used are comprehensive and include various financial and personal attributes of borrowers, credit history metrics, and loan characteristics. This rich feature set is designed to optimize the model's ability to detect fraud and anomalies.
    The only dependency we have in this file is we need to have the Final Prepared Data from Functions to Prepare Data for Model Training (Fraud and Anomaly).ipynb
    Features used in the XGBoost model:
    ['PRD_GIFT', 'DOWNPAYMENT', 'BLTV', 'TDSR', 'GDSR', 'PRK_PROPERTY_AGE', 'Num_Borrowers', 'DAYS_TO_CLOSING', 'Employment_Period_Mean', 'Employment_Period_Range', 'Age_Mean', 'Age_Range', 'Annual_Income_Mean', 'Annual_Income_Range', 'Total_Income_Mean', 'Total_Income_Range', 'PRD_FOREIGN_BORROWER', 'CDN_Resident_Pct', 'BOR_BKRPT_FLAG', 'SELF_EMPLOYED_FLAG', 'EMP_Sal_Pct', 'EMP_Oth_Pct', 'EMP_Mult_Pct', 'EMP_NonStandard_Pct', 'CRD_MONTHS_Mean', 'CRD_MONTHS_Range', 'CRD_OPEN_TRADE_Mean', 'CRD_SCORE_Mean', 'CRD_SCORE_Range', 'Source_Broker', 'Source_MDO', 'Cons_New', 'MLS_Y', 'Prop_Apartment', 'Prop_Detached', 'Prop_Mobile_Minihome', 'Prop_Row_House', 'MIN_DIST', 'MAX_DIST', 'MIN_DWELLING_DIFF', 'MAX_DWELLING_DIFF', 'MIN_HH_INC_DIFF', 'MAX_HH_INC_DIFF', 'MIN_INC_DIFF', 'MAX_INC_DIFF', 'OPEN_MORT_ALL_BOR', 'ANOMALY']


# 3. Function to Prep the Batch File (Using External Authentication).ipynb
     In this notebook, I ensure that the necessary CSV files are available in the same directory for seamless data preparation. These files include:
    • BorPostalCodes_ann.csv
    • PropPostalCodes_ann.csv
    • Manifold_ann.csv
    • postal_code.csv
    For this process, you need to ensure that your username in Snowflake data pull function is correctly configured. If not, the wrong token might be generated, leading to errors in authentication. This step is crucial for accessing the daily batch data securely and accurately.


# 4. Function to Prep the Batch File (Manual).ipynb
     In this notebook, I ensure that the necessary CSV files are available in the same directory for seamless data preparation. These files include:
    • BorPostalCodes_ann.csv
    • PropPostalCodes_ann.csv
    • Manifold_ann.csv
    • postal_code.csv
    In this approach, I manually execute the updated_query.sql in Snowflake to retrieve the necessary data. I need the Modeling_Redact_Exempt_Role role for this, as it grants me access to sensitive data like postal codes and annual incomes. After running the query, I download the data in CSV format and read it into the notebook for further processing.
# 5. Batch Prediction.ipynb
    In the batch prediction file, I use the data prepared in the previous steps, specifically files named in the format Final_Prepared_Data_XXXX-XX-XX.csv.
    I also implement the get_reasons function to identify the top 5 contributors to the SHAP value and their respective scores. This is instrumental in understanding the model's decision-making process.
    Another key function, shortstop, is used to identify any red flags in the batch data, which is crucial for pinpointing potential fraud or anomalies in the transactions.
    The only dependency here is the model which is saved ("fraudmodel_xgb.model) should be in the same directory.

This detailed documentation provides a comprehensive understanding of each file's purpose, functionality, and interdependencies, alongside specific instructions and requirements for successful execution and analysis in the fraud and anomaly detection pipeline.


# HOW TO RUN?

## CASE 1: Train the Model 
    To train the model first you will need to run the "Functions to Prepare Data for Model Training (Fraud and Anomaly).ipynb" file to prepare the data and then you will run "Model Training.ipynb" it will save the model to the directory with name fraudmodel_xgb.model

## CASE 2: Get Batch Predictions
    In this case we have two different ways to run it-
    1) Run it using the external authentication file
        First you will need to run the "Function to Prep the Batch File (Using External Authentication).ipynb" file by changing the dates in the "updated_query.sql" on line 80 then we need to change the date in the code so that we have correctly named files. Then we run the batch_prediction file, also change the date variable over there so that it gets saved with the correct name on s3
    2) Run it Manually
        For this we will need to go to snowflake and run the query we have in the file "updated_query.sql". Don't forget to change the date on line 80. Download the produced results in csv and then put in jupyter notebook. 
        now go to the file "Function to Prep the Batch File (Manual).ipynb" read the correct csv file that you just saved and then the process is similar to 1)

# Future updates to be planned -
    1) Create a lambda instance to run the code daily
    2) Connect the lambda instance with Amazon email service provider and send the files to bussiness team 
    3) Also we need to update this email system with something else because it is not efficient in the long run

# NOTES

    People in the mailing list of Fraud files
        1) Santhosh Sharma
        2) Akram Alyass
        3) Ashley Darling
        4) Jaco Habig
        5) Susan Savery
    
    Also when you run the files the variable that need to be updated are -
    1)"batch", "key" -> batch_prediction.ipynb
    2)"pod_received_date_start", "pod_received_date_end", "sdf" -> functions for data prep.ipynb (the sdf variable at the start where we read the csv file)
    3) line 80 on the updated_query.sql  