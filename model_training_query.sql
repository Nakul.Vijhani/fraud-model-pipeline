WITH borrower_credit AS (
    select
        borrower_credit_sid,
        round(crd_months, 1) as CRD_MONTHS,
        round(crd_score, 1) as CRD_SCORE,
        round(crd_open_trade, 1) as CRD_OPEN_TRADE,
        crd_bankruptcy_ind
    from
        DW.DM_POL.DMP_BORROWER_CREDIT_DIM
),
borrower AS (
    select
        DMP_BORROWER_DIM.borrower_sid,
        DMP_BORROWER_DIM.policy_sid as b_pol_sid,
        bor_age_an as BOR_AGE,
        cdn_resident,
        bor_annual_income_an as BOR_ANNUAL_INCOME,
        bor_total_income_an as BOR_TOTAL_INCOME,
        bor_emp_period,
        bor_income_type,
        bor_is_self_employed
    from
        DW.DM_POL.DMP_BORROWER_DIM
        join modelingrnd.borrower_an on DMP_BORROWER_DIM.borrower_sid = modelingrnd.borrower_an.borrower_sid
        and DMP_BORROWER_DIM.policy_sid = modelingrnd.borrower_an.policy_sid
),
collateral AS (
    select
        collateral_sid as coltl_sid,
        postal_code as coll_postal_code,
        curr_postal_code,
        prk_cons_type,
        prk_mls,
        prk_property_type,
        prk_property_age,
        prk_occupancy
    from
        DW.DM_POL.DMP_COLLATERAL_DIM
),
policy AS (
    select
        loan_type,
        policy_sid,
        risk_dtl_sid,
        time_mth_sid,
        product_sid,
        borrower_1_sid,
        borrower_2_sid,
        borrower_3_sid,
        borrower_4_sid,
        borrower_5_sid,
        borrower_credit_e_1_sid,
        borrower_credit_e_2_sid,
        borrower_credit_e_3_sid,
        borrower_credit_e_4_sid,
        borrower_credit_e_5_sid,
        borrower_credit_t_1_sid,
        borrower_credit_t_2_sid,
        borrower_credit_t_3_sid,
        borrower_credit_t_4_sid,
        borrower_credit_t_5_sid,
        prd_gift,
        downpayment,
        bltv,
        TO_CHAR(pod_received_date, 'YYYY-MM-DD HH24:MI:SS') AS POD_RECEIVED_DATE,
        pod_source_of_buss_type,
        tdsr,
        gdsr,
        collateral_sid,
        prk_purchase_price,
        prd_oth_mtg_amt,
        prd_base_loan_amt,
        RECEIVED_DAY_SID
    from
        DW.DM_POL.DMP_POLICY_DETAIL_FACT
    where
        time_mth_sid = 202312
        and product_sid = 29 -- corresponds to 1&2 unit
       
   
-- Adjust to include CREAT_TIME
),
risk AS (
    select
        risk_dtl_sid AS risk_sid,
        prd_loan_purpose,
        prd_charge_type,
        prd_foreign_borrower,
        TO_CHAR(prd_closing_date, 'YYYY-MM-DD HH24:MI:SS') AS PRD_CLOSING_DATE
    from
        DW.DM_POL.DMP_RISK_DETAIL_DIM
),
lob AS (
    select
        lob_sid,
        lruplus_flag,
        policy_sid as lb_pol_sid,
        application_number as sagen_ref_num
    from
        DW.DM_POL.DMP_POLICY_DIM
),
siu AS (
    WITH FINAL AS (
        select
            distinct siu_sid as FINAL_SIU_SID,
            sad_appl_num,
            max(sad_final_status) as STATUS
        from
            (
                select
                    distinct SIU_SID,
                    SAD_APPL_NUM,
                    SAD_FINAL_STATUS
                from
                    modelingrnd.vw_dmp_siu_application_dtl
                where
                    sad_appl_num is not null
                    and (
                        sad_final_status is null
                        or sad_final_status = 'M'
                    )
            )
        group by
            siu_sid,
            sad_appl_num
    ),
    APP AS (
        select
            policy_sid as app_policy_sid,
            application_number
        from
            DW.DM_POL.dmp_policy_dim
    )
    select
        SIU_SID,
        SAD_APPL_NUM,
        1 as SUS,
        app_policy_sid
    from
        modelingrnd.vw_dmp_siu_master
        join FINAL on siu_sid = final_siu_sid
        join APP on SAD_APPL_NUM = application_number
    WHERE
        (
            (
                siu_created_date >= '2019-07-01'
                AND status = 'M'
            )
            OR (siu_created_date < '2019-07-01')
        )
),
borrower_postal_code AS (
	SELECT 
		T1.BOR_SID,
		T1.POSTAL_CODE
	FROM (
		SELECT 
		BORROWER_1_SID BOR_SID, 
		BOR_1_POSTAL_CODE POSTAL_CODE 
		FROM DM_DBO.VW_DMP_BORROWER_1_DIM
		WHERE BORROWER_1_SID IS NOT NULL AND BOR_1_POSTAL_CODE IS NOT NULL) as T1
	FULL OUTER JOIN (
		SELECT 
		BORROWER_2_SID BOR_SID, 
		BOR_2_POSTAL_CODE POSTAL_CODE 
		FROM DM_DBO.VW_DMP_BORROWER_2_DIM
		WHERE BORROWER_2_SID IS NOT NULL AND BOR_2_POSTAL_CODE IS NOT NULL) as T2
		ON (T1.BOR_SID = T2.BOR_SID and T1.POSTAL_CODE = T2.POSTAL_CODE)
	FULL OUTER JOIN (
		SELECT 
		BORROWER_3_SID BOR_SID, 
		BOR_3_POSTAL_CODE POSTAL_CODE 
		FROM DM_DBO.VW_DMP_BORROWER_3_DIM
		WHERE BORROWER_3_SID IS NOT NULL AND BOR_3_POSTAL_CODE IS NOT NULL) as T3
		ON (T1.BOR_SID = T3.BOR_SID and T1.POSTAL_CODE = T3.POSTAL_CODE)
	FULL OUTER JOIN (
		SELECT 
		BORROWER_4_SID BOR_SID, 
		BOR_4_POSTAL_CODE POSTAL_CODE 
		FROM DM_DBO.VW_DMP_BORROWER_4_DIM
		WHERE BORROWER_4_SID IS NOT NULL AND BOR_4_POSTAL_CODE IS NOT NULL) as T4
		ON (T1.BOR_SID = T4.BOR_SID and T1.POSTAL_CODE = T4.POSTAL_CODE)
	FULL OUTER JOIN (
		SELECT 
		BORROWER_5_SID BOR_SID, 
		BOR_5_POSTAL_CODE POSTAL_CODE 
		FROM DM_DBO.VW_DMP_BORROWER_5_DIM
		WHERE BORROWER_5_SID IS NOT NULL AND BOR_5_POSTAL_CODE IS NOT NULL) as T5
		ON (T1.BOR_SID = T5.BOR_SID and T1.POSTAL_CODE = T5.POSTAL_CODE)
),
borrower_age_income AS (
	SELECT 
		 POLICY_SID,
		 BORROWER_SID,
		 BOR_AGE,
		 BOR_ANNUAL_INCOME,
		 BOR_TOTAL_INCOME
	FROM DM_POL.DMP_BORROWER_DIM 
),
CRD_No_NULLs AS (
	SELECT *
	FROM DM_POL.DMP_BORROWER_CREDIT_DIM
	WHERE CRD_NO_OF_OPEN_MORT_TRADES IS NOT NULL
),
CRD_MIN AS (
	SELECT
		CRD_CREDIT_ID,
		count(CRD_NO_OF_OPEN_MORT_TRADES) CRD_PULL_COUNTS,
		MIN(CREATED_DATE) MIN_CREATED_DATE
	FROM CRD_No_NULLs --DM_POL.DMP_BORROWER_CREDIT_DIM
	GROUP BY CRD_CREDIT_ID
),
DMP_BORROWER_CREDIT_DIM_ORIG AS (
SELECT 
	BC.BORROWER_CREDIT_SID,
	BC.CRD_CREDIT_ID,
	BC.CRD_NO_OF_OPEN_MORT_TRADES
FROM DM_POL.DMP_BORROWER_CREDIT_DIM BC
INNER JOIN CRD_MIN md on BC.CRD_CREDIT_ID = md.CRD_CREDIT_ID
WHERE BC.CRD_NO_OF_OPEN_MORT_TRADES IS NOT NULL
)
SELECT
   
    
    BC1.CRD_NO_OF_OPEN_MORT_TRADES B1_OM, 
	BC2.CRD_NO_OF_OPEN_MORT_TRADES B2_OM, 
	BC3.CRD_NO_OF_OPEN_MORT_TRADES B3_OM, 
	BC4.CRD_NO_OF_OPEN_MORT_TRADES B4_OM, 
	BC5.CRD_NO_OF_OPEN_MORT_TRADES B5_OM, 
    p.RECEIVED_DAY_SID,
    
    prd_loan_purpose,
    p.policy_sid,
    p.risk_dtl_sid,
    p.time_mth_sid,
    product_sid,
    borrower_1_sid,
    borrower_2_sid,
    borrower_3_sid,
    borrower_4_sid,
    borrower_5_sid,
    borrower_credit_e_1_sid,
    borrower_credit_e_2_sid,
    borrower_credit_e_3_sid,
    borrower_credit_e_4_sid,
    borrower_credit_e_5_sid,
    borrower_credit_t_1_sid,
    borrower_credit_t_2_sid,
    borrower_credit_t_3_sid,
    borrower_credit_t_4_sid,
    borrower_credit_t_5_sid,
    prd_gift,
    downpayment,
    bltv,
    pod_received_date,
    pod_source_of_buss_type,
    tdsr,
    gdsr,
    collateral_sid,
    sagen_ref_num,
    lob_sid,
    collateral.coll_postal_code,
    curr_postal_code,
    prk_cons_type,
    prk_mls,
    prk_property_type,
    prk_property_age,
    --b1.bor_age AS bor1_age,
    b1.cdn_resident AS bor1_cdn_resident,
    --b1.bor_annual_income AS bor1_annual_income,
    --b1.bor_total_income AS bor1_total_income,
    b1.bor_emp_period AS bor1_emp_period,
    b1.bor_income_type AS bor1_income_type,
    b1.bor_is_self_employed AS bor1_is_self_employed,
	bcode1.POSTAL_CODE AS bor1_postal_code,
	b1_age_income.BOR_AGE bor1_AGE,
	b1_age_income.BOR_ANNUAL_INCOME bor1_ANNUAL_INCOME,
	b1_age_income.BOR_TOTAL_INCOME bor1_TOTAL_INCOME,
    
    --b2.bor_age AS bor2_age,
    b2.cdn_resident AS bor2_cdn_resident,
   -- b2.bor_annual_income AS bor2_annual_income,
    --b2.bor_total_income AS bor2_total_income,
    b2.bor_emp_period AS bor2_emp_period,
    b2.bor_income_type AS bor2_income_type,
    b2.bor_is_self_employed AS bor2_is_self_employed,
	bcode2.POSTAL_CODE AS bor2_postal_code,
    b2_age_income.BOR_AGE bor2_AGE,
	b2_age_income.BOR_ANNUAL_INCOME bor2_ANNUAL_INCOME,
	b2_age_income.BOR_TOTAL_INCOME bor2_TOTAL_INCOME,
    
    --b3.bor_age AS bor3_age,
    b3.cdn_resident AS bor3_cdn_resident,
    --b3.bor_annual_income AS bor3_annual_income,
    --b3.bor_total_income AS bor3_total_income,
    b3.bor_emp_period AS bor3_emp_period,
    b3.bor_income_type AS bor3_income_type,
    bcode3.POSTAL_CODE AS bor3_postal_code,
    b3.bor_is_self_employed AS bor3_is_self_employed,
    b3_age_income.BOR_AGE bor3_AGE,
	b3_age_income.BOR_ANNUAL_INCOME bor3_ANNUAL_INCOME,
	b3_age_income.BOR_TOTAL_INCOME bor3_TOTAL_INCOME,
    
    --b4.bor_age AS bor4_age,
    b4.cdn_resident AS bor4_cdn_resident,
    --b4.bor_annual_income AS bor4_annual_income,
    --b4.bor_total_income AS bor4_total_income,
    b4.bor_emp_period AS bor4_emp_period,
    b4.bor_income_type AS bor4_income_type,
    b4.bor_is_self_employed AS bor4_is_self_employed,
	bcode4.POSTAL_CODE AS bor4_postal_code,
    b4_age_income.BOR_AGE bor4_AGE,
	b4_age_income.BOR_ANNUAL_INCOME bor4_ANNUAL_INCOME,
	b4_age_income.BOR_TOTAL_INCOME bor4_TOTAL_INCOME,
    
    --b5.bor_age AS bor5_age,
    b5.cdn_resident AS bor5_cdn_resident,
    --b5.bor_annual_income AS bor5_annual_income,
    --b5.bor_total_income AS bor5_total_income,
    b5.bor_emp_period AS bor5_emp_period,
    b5.bor_income_type AS bor5_income_type,
    b5.bor_is_self_employed AS bor5_is_self_employed,
	bcode5.POSTAL_CODE AS bor5_postal_code,
    b5_age_income.BOR_AGE bor5_AGE,
	b5_age_income.BOR_ANNUAL_INCOME bor5_ANNUAL_INCOME,
	b5_age_income.BOR_TOTAL_INCOME bor5_TOTAL_INCOME,
    
    prd_foreign_borrower,
    prd_closing_date,
    e1.crd_months AS bor1_e_crd_months,
    e1.crd_score AS bor1_e_crd_score,
    e1.crd_bankruptcy_ind AS bor1_e_crd_bankruptcy_indicator,
    e1.crd_open_trade AS bor1_e_crd_open_trade,
    t1.crd_months AS bor1_t_crd_months,
    t1.crd_score AS bor1_t_crd_score,
    t1.crd_bankruptcy_ind AS bor1_t_crd_bankruptcy_indicator,
    t1.crd_open_trade AS bor1_t_crd_open_trade,
    e2.crd_months AS bor2_e_crd_months,
    e2.crd_score AS bor2_e_crd_score,
    e2.crd_bankruptcy_ind AS bor2_e_crd_bankruptcy_indicator,
    e2.crd_open_trade AS bor2_e_crd_open_trade,
    t2.crd_months AS bor2_t_crd_months,
    t2.crd_score AS bor2_t_crd_score,
    t2.crd_bankruptcy_ind AS bor2_t_crd_bankruptcy_indicator,
    t2.crd_open_trade AS bor2_t_crd_open_trade,
    e3.crd_months AS bor3_e_crd_months,
    e3.crd_score AS bor3_e_crd_score,
    e3.crd_bankruptcy_ind AS bor3_e_crd_bankruptcy_indicator,
    e3.crd_open_trade AS bor3_e_crd_open_trade,
    t3.crd_months AS bor3_t_crd_months,
    t3.crd_score AS bor3_t_crd_score,
    t3.crd_bankruptcy_ind AS bor3_t_crd_bankruptcy_indicator,
    t3.crd_open_trade AS bor3_t_crd_open_trade,
    e4.crd_months AS bor4_e_crd_months,
    e4.crd_score AS bor4_e_crd_score,
    e4.crd_bankruptcy_ind AS bor4_e_crd_bankruptcy_indicator,
    e4.crd_open_trade AS bor4_e_crd_open_trade,
    t4.crd_months AS bor4_t_crd_months,
    t4.crd_score AS bor4_t_crd_score,
    t4.crd_bankruptcy_ind AS bor4_t_crd_bankruptcy_indicator,
    t4.crd_open_trade AS bor4_t_crd_open_trade,
    e5.crd_months AS bor5_e_crd_months,
    e5.crd_score AS bor5_e_crd_score,
    e5.crd_bankruptcy_ind AS bor5_e_crd_bankruptcy_indicator,
    e5.crd_open_trade AS bor5_e_crd_open_trade,
    t5.crd_months AS bor5_t_crd_months,
    t5.crd_score AS bor5_t_crd_score,
    t5.crd_bankruptcy_ind AS bor5_t_crd_bankruptcy_indicator,
    t5.crd_open_trade AS bor5_t_crd_open_trade,
    siu.siu_sid,
    siu.sad_appl_num,
    siu.sus
FROM
    policy p

    LEFT OUTER JOIN DMP_BORROWER_CREDIT_DIM_ORIG BC1 ON p.BORROWER_CREDIT_T_1_SID = BC1.BORROWER_CREDIT_SID
	LEFT OUTER JOIN DMP_BORROWER_CREDIT_DIM_ORIG BC2 ON p.BORROWER_CREDIT_T_2_SID = BC2.BORROWER_CREDIT_SID
	LEFT OUTER JOIN DMP_BORROWER_CREDIT_DIM_ORIG BC3 ON p.BORROWER_CREDIT_T_3_SID = BC3.BORROWER_CREDIT_SID
	LEFT OUTER JOIN DMP_BORROWER_CREDIT_DIM_ORIG BC4 ON p.BORROWER_CREDIT_T_4_SID = BC4.BORROWER_CREDIT_SID
	LEFT OUTER JOIN DMP_BORROWER_CREDIT_DIM_ORIG BC5 ON p.BORROWER_CREDIT_T_5_SID = BC5.BORROWER_CREDIT_SID
    LEFT OUTER JOIN lob ON p.POLICY_SID = lob.lb_pol_sid
    --JOIN DW.DM_DBO.VW_DMP_TIME_DAY_REC_DIM A16 ON p.RECEIVED_DAY_SID = A16.RECEIVED_DAY_SID
    
    --LEFT JOIN lob ON p.policy_sid = lob.lb_pol_sid
    LEFT JOIN collateral on p.collateral_sid = collateral.coltl_sid -- LEFT JOIN credit AS c1 on policy.borrower_1_sid = c1.bor_id
    -- LEFT JOIN credit AS c2 on policy.borrower_2_sid = c2.bor_id
    -- LEFT JOIN credit AS c3 on policy.borrower_3_sid = c3.bor_id
    -- LEFT JOIN credit AS c4 on policy.borrower_4_sid = c4.bor_id
    -- LEFT JOIN credit AS c5 on policy.borrower_5_sid = c5.bor_id
    LEFT JOIN borrower AS b1 on p.BORROWER_1_SID = b1.BORROWER_SID
    and p.POLICY_SID = b1.b_pol_sid
    LEFT JOIN borrower AS b2 on p.BORROWER_2_SID = b2.BORROWER_SID
    and p.POLICY_SID = b2.b_pol_sid
    LEFT JOIN borrower AS b3 on p.BORROWER_3_SID = b3.BORROWER_SID
    and p.POLICY_SID = b3.b_pol_sid
    LEFT JOIN borrower AS b4 on p.BORROWER_4_SID = b4.BORROWER_SID
    and p.POLICY_SID = b4.b_pol_sid
    LEFT JOIN borrower AS b5 on p.BORROWER_5_SID = b5.BORROWER_SID
    and p.POLICY_SID = b5.b_pol_sid
    LEFT JOIN risk ON p.risk_dtl_sid = risk.risk_sid
    LEFT JOIN borrower_credit AS e1 on p.borrower_credit_e_1_sid = e1.borrower_credit_sid
    LEFT JOIN borrower_credit AS t1 on p.borrower_credit_t_1_sid = t1.borrower_credit_sid
    LEFT JOIN borrower_credit AS e2 on p.borrower_credit_e_2_sid = e2.borrower_credit_sid
    LEFT JOIN borrower_credit AS t2 on p.borrower_credit_t_2_sid = t2.borrower_credit_sid
    LEFT JOIN borrower_credit AS e3 on p.borrower_credit_e_3_sid = e3.borrower_credit_sid
    LEFT JOIN borrower_credit AS t3 on p.borrower_credit_t_3_sid = t3.borrower_credit_sid
    LEFT JOIN borrower_credit AS e4 on p.borrower_credit_e_4_sid = e4.borrower_credit_sid
    LEFT JOIN borrower_credit AS t4 on p.borrower_credit_t_4_sid = t4.borrower_credit_sid
    LEFT JOIN borrower_credit AS e5 on p.borrower_credit_e_5_sid = e5.borrower_credit_sid
    LEFT JOIN borrower_credit AS t5 on p.borrower_credit_t_5_sid = t5.borrower_credit_sid
    LEFT JOIN siu ON p.POLICY_SID = siu.app_policy_sid
	--*
	LEFT JOIN borrower_postal_code AS bcode1 on p.BORROWER_1_SID = bcode1.BOR_SID
    LEFT JOIN borrower_postal_code AS bcode2 on p.BORROWER_2_SID = bcode2.BOR_SID
    LEFT JOIN borrower_postal_code AS bcode3 on p.BORROWER_3_SID = bcode3.BOR_SID
    LEFT JOIN borrower_postal_code AS bcode4 on p.BORROWER_4_SID = bcode4.BOR_SID
    LEFT JOIN borrower_postal_code AS bcode5 on p.BORROWER_5_SID = bcode5.BOR_SID
    
	LEFT JOIN borrower_age_income AS b1_age_income on p.BORROWER_1_SID = b1_age_income.BORROWER_SID
    and p.POLICY_SID = b1_age_income.POLICY_SID
	LEFT JOIN borrower_age_income AS b2_age_income on p.BORROWER_2_SID = b2_age_income.BORROWER_SID
    and p.POLICY_SID = b2_age_income.POLICY_SID
    LEFT JOIN borrower_age_income AS b3_age_income on p.BORROWER_3_SID = b3_age_income.BORROWER_SID
    and p.POLICY_SID = b3_age_income.POLICY_SID
	LEFT JOIN borrower_age_income AS b4_age_income on p.BORROWER_4_SID = b4_age_income.BORROWER_SID
    and p.POLICY_SID = b4_age_income.POLICY_SID
    LEFT JOIN borrower_age_income AS b5_age_income on p.BORROWER_5_SID = b5_age_income.BORROWER_SID
    and p.POLICY_SID = b5_age_income.POLICY_SID
WHERE
    LOB_SID = 1 
    --AND TIME_DATE_R >'2011-08-01'
    AND (BC1.CRD_NO_OF_OPEN_MORT_TRADES IS NOT NULL
        or BC2.CRD_NO_OF_OPEN_MORT_TRADES IS NOT NULL
        or BC3.CRD_NO_OF_OPEN_MORT_TRADES IS NOT NULL
        or BC4.CRD_NO_OF_OPEN_MORT_TRADES IS NOT NULL
        or BC5.CRD_NO_OF_OPEN_MORT_TRADES IS NOT NULL)
     --
     
     ;